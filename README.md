# Multipass

Support pour une petite présentation de Multipass by Canonical, essentiellement basée sur des démos de l'outil.
durée : 15 Minutes

## Introduction

Dans notre métier de mercenaire, il faut savoir s'adapter a toutes les situations. Nous n'avons pas toujours les mêmes environnement de travail, pas les mêmes accès a l'infrastructure (et surtout en ce moment, certaines boites n'ont meme pas de vpn pour les prestataires) et donc on peut etre amener a travailler sur des systemes qu'il faut simuler sur notre poste, afin de travailler au plus prêt de la cible, et eviter de developper directement sur le serveur. Une des techno pour se faire, est Multipass, qui va permettre de créer des VM ubuntu sur notre ordinateur. 

## Installez Multipass (multipass.run) 

...et lancez votre premiere machine, pour tester une commande, un script shell, par exemple : 

### pour juste avoir une VM sous Ubuntu (pet)

```
multipass shell
```

Cette commande va lancer notre vm principale. La vm "primary" est un peu spéciale, celle ci a notamment un lien avec votre disque, c'est une sorte de bastion, sur laquelle on va installer des logiciels, mais qui ne va pas servir de labo ou de machine jetable.

tips : on peut donc y installer ansible, par exemple, et provisionner les autres VM à partir de celle-ci

tips : isolation, c'est la clé ! Evitons d'installer plein de trucs sur nos ordinateurs, pourissons plutôt des VM, elles sont faites pour ca.

### créer une vm et faire tourner des scripts dessus 

La CLI multipass permet de créer autant de VM que l'on veut, avec plusieurs options (sizing, points de montage, etc) et on peut donc soit lancer des vm a la main, soit se préparer un petit script pour se créer une infra, sur sa machine.

Exemple : le script de Philippe Charriere 

```
less create.cluster.sh
```


### lancer avec cloudinit

Ca n'est pas l'objet de cette présentation, mais Cloud Init est un standard imaginé il y a quelques années déja, qui permet de fournir à des machines virtuelles en devenir un état : c'est donc un fichier, et dans ce fichier, on va préciser qu'on veut rajouter un utilisateur, qu'on veut installer tel logiciel, ou créer un filesystem. Il existe tout un tas de modules et d'options, qui fait qu'on peut aller très loin, et donc créer des machines virtuelles "clones" très rapidement, et sans se tromper. Multipass supporte cette implémentation, et donc, en créant une vm et en lui passant l'option "cloud-init", on crée une VM pré-configurée.

Pratique si on veut pouvoir tester des choses potentiellement destructrices sur la machines (j'ai récemment joué avec des scripts ansible qui démontent des points de montage sur le disque, et un tel environnement aide grandement à ne pas se retrouver en pleurs, roulé en boule sous son bureau parce qu'on a vidé une machine de prod)
 
```
multipass launch -n serveur-web --cloud-init cloud-config.yaml
``` 
